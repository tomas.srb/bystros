# BystrOs demonstration

List files in current dir in long format (currently simplified)

`./ls -l`

List files in current dir in long format sorted by size

`./ls -l | ./sort -k /stat/size`

List files in current dir in long format sorted by mtime (newest first)

`./ls -l | ./sort -r -k /stat/mtime`

Display all information about files in current dir in yaml format

`./ls -l | ./sort -r -k /stat/size | ./2yaml`

# Requirements

- python2-pyyaml
- python2-jsonpointer (Library to resolve JSON Pointers according to RFC 6901.)