#!/usr/bin/env python2

import os
import bystros
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--long', action='store_true')

args = parser.parse_args()

for filename in os.listdir('.'):
    fileinfo = os.lstat(filename)
    data = {
        '_type': 'ls',
        '_params': vars(args),
        'name': filename,
        'stat': {
            'mode': fileinfo.st_mode,
            'ino': fileinfo.st_ino,
            'dev': fileinfo.st_dev,
            'nlink': fileinfo.st_nlink,
            'uid': fileinfo.st_uid,
            'gid': fileinfo.st_gid,
            'size': fileinfo.st_size,
            'atime': fileinfo.st_atime,
            'mtime': fileinfo.st_mtime,
            'ctime': fileinfo.st_ctime,
        }
    }
    bystros.stdout.write(data)
