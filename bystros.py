#!/usr/bin/env python2

import sys
import os
import subprocess
import json
import fcntl

class Stdout:
    def __init__(self):
        self.formaters = {}

    def write(self, data):
        output = self.get_formater(data['_type']).stdin if sys.stdout.isatty() else sys.stdout
        output.write(json.dumps(data, separators=(',', ':')) + '\n')
        output.flush()

    def get_formater(self, name):
        executable = os.path.join('.', name + '-print')
        if not os.path.isfile(executable):
            raise Exception("Can't process record with type '%s'.", name)
        if name not in self.formaters:
            self.formaters[name] = subprocess.Popen(executable, stdin=subprocess.PIPE)
        return self.formaters[name]

    def __del__(self):
        for name, formater in self.formaters.items():
            del self.formaters[name]
            formater.stdin.close()
            formater.wait()

stdout = Stdout()

class Stdin:
    def read(self):
        return json.loads(sys.stdin.readline())

    def __iter__(self):
        return self

    def next(self):
        line = sys.stdin.readline()
        if not line:
            raise StopIteration()
        return json.loads(line)

stdin = Stdin()
